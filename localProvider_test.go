package main

import (
	"testing"

	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/test"
)

func Test_signing_rsa4096(t *testing.T) {
	localProvider := new(LocalCryptoProvider)
	if !test.Sign_Testing_Rsa4096(localProvider) {
		t.Error()
	}
}

func Test_encryption_aes256(t *testing.T) {
	localProvider := new(LocalCryptoProvider)
	if !test.Encryption_Testing_Aes256(localProvider) {
		t.Error()
	}
}
